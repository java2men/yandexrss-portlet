package ru.admomsk.uikt;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;


public class Item {
	private String title = "";
	private String link = "";
	private String description = "";
	private String author = "";
	//private List<String> enclosures;
	private String pubDate = "";
	private String yandexGenre = "";
	private String yandexFullText = "";
	
	public Item() {
		//enclosures = new ArrayList<String>();
	}
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getLink() {
		return link;
	}
	public void setLink(String link) {
		this.link = link;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	/*
	public List<String> getEnclosures() {
		return enclosures;
	}
	public void addEnclosure(String enclosure) {
		this.enclosures.add(enclosure);
	}
	public void setEnclosures(List<String> enclosures) {
		this.enclosures = enclosures;
	}
	*/
	public String getPubDate() {
		return pubDate;
	}
	public void setPubDate(String pubDate) {
		//this.pubDate = pubDate;
		
		/*написать шаблон для даты как в яндексе*/
		
		Locale locIn = new Locale("ru","RU");
		DateFormat dfIn = new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss ZZZ", locIn);
		Date dateIn = null;
		try {
			dateIn = dfIn.parse(pubDate);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		Locale locOut = new Locale("en","EN");
		DateFormat dfOut = new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss ZZZ", locOut);
		this.pubDate = dfOut.format(dateIn);
		System.out.println("pubDate = " + this.pubDate);
		
	}
	public String getYandexGenre() {
		return yandexGenre;
	}
	public void setYandexGenre(String yandexGenre) {
		this.yandexGenre = yandexGenre;
	}
	public String getYandexFullText() {
		return yandexFullText;
	}
	public void setYandexFullText(String yandexFullText) {
		this.yandexFullText = yandexFullText;
	}
	
	public String asXML() {
		StringBuilder sb = new StringBuilder();
		sb.append("<item>").append("\n");
		sb.append("<title>").append(title).append("</title>").append("\n");
		sb.append("<link>").append(link).append("</link>").append("\n");
		sb.append("<description>").append(description).append("</description>").append("\n");
		/*
		for (String enclosure:enclosures) {
			sb.append("<enclosure ").append(enclosure).append(" />").append("\n");
		}
		*/
		sb.append("<pubDate>").append(pubDate).append("</pubDate>").append("\n");
		sb.append("<yandex:genre>message</yandex:genre>").append("\n");
		sb.append("<yandex:full-text>").append(yandexFullText).append("</yandex:full-text>").append("\n");
		
		sb.append("</item>").append("\n");
		return sb.toString();
	}
}
