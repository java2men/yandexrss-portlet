package ru.admomsk.uikt.beans;

import java.io.IOException;
import java.io.Serializable;
import java.net.MalformedURLException;
import java.net.URL;

import javax.portlet.PortletPreferences;
import javax.portlet.ReadOnlyException;
import javax.portlet.ValidatorException;

@SuppressWarnings("serial")
public class EditBean implements Serializable {
	
	private static final long serialVersionUID = 1453347212373892627L;

	private PortletPreferences portletPreferences;
	
	private String urlRssNewsPortal = "";
	private String validUrlRssNewsPortal = "";
	
	public String idle = "idle";
	public String ok = "ok";
	public String badUrl = "badUrl";
	
	public String getIdle() {
		return idle;
	}

	public void setIdle(String idle) {
		this.idle = idle;
	}

	public String getOk() {
		return ok;
	}

	public void setOk(String ok) {
		this.ok = ok;
	}

	public String getBadUrl() {
		return badUrl;
	}

	public void setBadUrl(String badUrl) {
		this.badUrl = badUrl;
	}

	public PortletPreferences getPortletPreferences() {
		return portletPreferences;
	}

	public void setPortletPreferences(PortletPreferences portletPreferences) {
		this.portletPreferences = portletPreferences;
	}

	public String getUrlRssNewsPortal() {
		if (portletPreferences == null) return null;
		urlRssNewsPortal = (String)portletPreferences.getValue("urlRssNewsPortal", "");
		return urlRssNewsPortal;
	}

	public void setUrlRssNewsPortal(String urlRssNewsPortal) throws ReadOnlyException, ValidatorException, IOException {
		//this.urlRssNewsPortal = urlRssNewsPortal;
		
		if (urlRssNewsPortal == null) {
			urlRssNewsPortal = "";
		}
		this.urlRssNewsPortal = urlRssNewsPortal;
		portletPreferences.setValue("urlRssNewsPortal", this.urlRssNewsPortal);
		portletPreferences.store();
	}
	
	public String getValidUrlRssNewsPortal() {
		return validUrlRssNewsPortal;
	}
	public void validateUrlRssNewsPortal() {
		validUrlRssNewsPortal = ok;
		
		if (getUrlRssNewsPortal().equals("")) {
			validUrlRssNewsPortal = idle;
			return;
		}
		
		try {
			new URL(getUrlRssNewsPortal());
		} catch (MalformedURLException e) {
			validUrlRssNewsPortal = badUrl;
			e.printStackTrace();
		}
		
	}
	
	
	
}
