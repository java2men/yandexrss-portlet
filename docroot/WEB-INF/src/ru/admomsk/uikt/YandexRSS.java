package ru.admomsk.uikt;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.portlet.GenericPortlet;
import javax.portlet.PortletException;
import javax.portlet.PortletRequestDispatcher;
import javax.portlet.PortletSession;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import ru.admomsk.uikt.beans.EditBean;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.HtmlUtil;
import com.liferay.portal.kernel.xml.Document;
import com.liferay.portal.kernel.xml.DocumentException;
import com.liferay.portal.kernel.xml.Element;
import com.liferay.portal.kernel.xml.SAXReaderUtil;

/**
 * Portlet implementation class YandexRSS
 */
public class YandexRSS extends GenericPortlet {
	
	protected String editJSP;
    protected String viewJSP;

    private static Log _log = LogFactoryUtil.getLog(YandexRSS.class);
	
	URL urlRssNewsPortal;
	//ResourceURL varYandexRss1;
	
    public void init() {
        editJSP = getInitParameter("edit-jsp");
        viewJSP = getInitParameter("view-jsp");
    }
    
    @Override
    public void serveResource(ResourceRequest request, ResourceResponse response)
    		throws PortletException, IOException {
    	
    	/*
    	Document document = null;
    	try {
    	*/
		String xml = toYandexXmlRSS(urlRssNewsPortal);
		/*
			document = SAXReaderUtil.read(xml);
			document.setXMLEncoding("windows-1251");
		*/
    	
		response.setCharacterEncoding("windows-1251");
		
		//отобразить в кодировке windows-1251
		PrintWriter pw = new PrintWriter(new OutputStreamWriter(response.getPortletOutputStream(), "windows-1251"), true);
		pw.print(new String(xml.getBytes("windows-1251"),"windows-1251"));
		pw.close();
			
			/*
			PrintWriter pw = new PrintWriter(new OutputStreamWriter(response.getPortletOutputStream(), "windows-1251"), true);
			pw.print(new String(document.asXML().getBytes("windows-1251"),"windows-1251"));
			pw.close();
			*/
    	/*	
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		*/
    }
    
    /*
    @ProcessAction(name = "editAction")
	public void editAction(ActionRequest actionRequest,ActionResponse actionResponse){	
    	System.out.println("i in editAction");
	}
    */
    
    /*
    public void processAction(
            ActionRequest actionRequest, ActionResponse actionResponse)
        throws IOException, PortletException {
    	System.out.println("i in processAction");
        //super.processAction(actionRequest, actionResponse);
    }
    */
    public void doEdit(
            RenderRequest renderRequest, RenderResponse renderResponse)
        throws IOException, PortletException {
        
    	PortletSession portletSession = renderRequest.getPortletSession();
    	
    	if ((EditBean)portletSession.getAttribute("editBean") == null){
    		portletSession.setAttribute("editBean", new EditBean());
    	}
    	
    	//Установка настроек портлета
    	((EditBean)portletSession.getAttribute("editBean")).setPortletPreferences(renderRequest.getPreferences());
		
    	if (renderRequest.getParameter("urlRssNewsPortal") != null) {
    		((EditBean)portletSession.getAttribute("editBean")).setUrlRssNewsPortal(renderRequest.getParameter("urlRssNewsPortal"));
    		((EditBean)portletSession.getAttribute("editBean")).validateUrlRssNewsPortal();
    	}
    	
		renderRequest.setAttribute("editBean", (EditBean)portletSession.getAttribute("editBean"));
    	
        include(editJSP, renderRequest, renderResponse);
    }
    
    public void doView(
            RenderRequest renderRequest, RenderResponse renderResponse)
        throws IOException, PortletException {
    	
        PortletSession portletSession = renderRequest.getPortletSession();
    	if ((EditBean)portletSession.getAttribute("editBean") == null){
    		portletSession.setAttribute("editBean", new EditBean());
    	}
    	//Установка настроек портлета
    	((EditBean)portletSession.getAttribute("editBean")).setPortletPreferences(renderRequest.getPreferences());
    	((EditBean)portletSession.getAttribute("editBean")).validateUrlRssNewsPortal();
    	urlRssNewsPortal = null;
    	if (((EditBean)portletSession.getAttribute("editBean")).getValidUrlRssNewsPortal().equals("ok")) {
    		urlRssNewsPortal = new URL(((EditBean)portletSession.getAttribute("editBean")).getUrlRssNewsPortal());
    	}
    	renderRequest.setAttribute("urlRssNewsPortal", urlRssNewsPortal);
    	
        include(viewJSP, renderRequest, renderResponse);
    }

    protected void include(
            String path, RenderRequest renderRequest,
            RenderResponse renderResponse)
        throws IOException, PortletException {

        PortletRequestDispatcher portletRequestDispatcher =
            getPortletContext().getRequestDispatcher(path);

        if (portletRequestDispatcher == null) {
            _log.error(path + " is not a valid include");
        }
        else {
            portletRequestDispatcher.include(renderRequest, renderResponse);
        }
    }
    
    private String toYandexXmlRSS(URL urlPortalXml) {
    	
		StringBuilder sbYandexXmlRSS = new StringBuilder();
		
		Document portalXml = null;
		ArrayList<Item> items = new ArrayList<Item>();
		
		try {
			portalXml = SAXReaderUtil.read(urlPortalXml);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		
		List<Element> results = portalXml.getRootElement().elements("result");
		for(Element result : results) {
			List<Element> roots = result.elements("root");
			for(Element root : roots) {
				List<Element> dynamicElements = root.elements("dynamic-element");
				Item item = new Item();
				for(Element dynamicElement : dynamicElements){
					if (dynamicElement.attributeValue("name").equals("text")) {
						item.setYandexFullText(HtmlUtil.extractText(dynamicElement.element("dynamic-content").getText()));
					}
					if (dynamicElement.attributeValue("name").equals("reserved-article-id")) {
						item.setLink(new StringBuilder("http://admomsk.ru/web/guest/news/-/asset_publisher/mh3W/content/").append(dynamicElement.element("dynamic-content").getText()).toString());
					}
					if (dynamicElement.attributeValue("name").equals("reserved-article-title")) {
						item.setTitle(dynamicElement.element("dynamic-content").getText());
					}
					if (dynamicElement.attributeValue("name").equals("reserved-article-description")) {
						item.setDescription(dynamicElement.element("dynamic-content").getText());
					}
					if (dynamicElement.attributeValue("name").equals("reserved-article-display-date")) {
						item.setPubDate(dynamicElement.element("dynamic-content").getText());
					}
				}
				items.add(item);
			}
		}
				
		//Собираем xml для Yandex
		//вроде как устанавливается в парсере
		sbYandexXmlRSS.append("<?xml version=\"1.0\" encoding=\"windows-1251\"?>").append("\n");
		try {
			sbYandexXmlRSS.append(new String("<rss xmlns:yandex=\"http://news.yandex.ru\" xmlns:media=\"http://search.yahoo.com/mrss/\" version=\"2.0\">\n".getBytes("windows-1251"), "windows-1251"));
		
			sbYandexXmlRSS.append(new String("<channel>\n".getBytes("windows-1251"), "windows-1251"));
			
			//Заёб с кодировкой!
			//В eclipse marketplace поставил "Properties Editor". Через "Properties Editor" получил Unicode русских строк, заменил, и на сервере liferay нормализовалась кодировка.     
			/*
			sbYandexXmlRSS.append(new String(new String("<title>Новости Администрации города Омска</title>\n".getBytes("UTF-8"), "UTF-8").getBytes("windows-1251"), "windows-1251"));
			sbYandexXmlRSS.append(new String("<link>http://www.admomsk.ru</link>\n".getBytes("windows-1251"), "windows-1251"));
			sbYandexXmlRSS.append(new String("<description>Новости Администрации города Омска</description>\n".getBytes("windows-1251"), "windows-1251"));
			*/
			sbYandexXmlRSS.append("<title>\u041d\u043e\u0432\u043e\u0441\u0442\u0438 \u0410\u0434\u043c\u0438\u043d\u0438\u0441\u0442\u0440\u0430\u0446\u0438\u0438 \u0433\u043e\u0440\u043e\u0434\u0430 \u041e\u043c\u0441\u043a\u0430</title>\n");
			sbYandexXmlRSS.append("<link>http://www.admomsk.ru</link>\n");
			sbYandexXmlRSS.append("<description>\u041d\u043e\u0432\u043e\u0441\u0442\u0438 \u0410\u0434\u043c\u0438\u043d\u0438\u0441\u0442\u0440\u0430\u0446\u0438\u0438 \u0433\u043e\u0440\u043e\u0434\u0430 \u041e\u043c\u0441\u043a\u0430</description>\n");
			
			sbYandexXmlRSS.append(new String("<yandex:logo>http://www.admomsk.ru/normal_logo.png</yandex:logo>\n".getBytes("windows-1251"), "windows-1251"));
			sbYandexXmlRSS.append(new String("<yandex:logo type=\"square\">http://www.admomsk.ru/square_logo180x180.png</yandex:logo>\n".getBytes("windows-1251"), "windows-1251"));
			
			for(Item item : items){
				sbYandexXmlRSS.append(item.asXML());
			}
			
			sbYandexXmlRSS.append("</channel>").append("\n");
			sbYandexXmlRSS.append("</rss>");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return sbYandexXmlRSS.toString();
	}

}
