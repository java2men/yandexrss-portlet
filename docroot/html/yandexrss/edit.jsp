<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>

<portlet:defineObjects />
<jsp:useBean id="editBean" class="ru.admomsk.uikt.beans.EditBean" scope="request"/>
<portlet:actionURL name="editAction" var="varEditAction" ></portlet:actionURL>

<div id="kindergarden">
	<form id="form_edit_status_docs" name="form_edit_yandex_rss" enctype="pplication/x-www-form-urlencoded" action="<portlet:renderURL />" method="post">
		<div class="form-all">
			
			<ul class="form-section">
				<li class="form-line">
					<label class="form-label-left">URL RSS-новостей портала<span class="form-required">*</span></label>
					<div class="form-input-wide">
						<span class="form-sub-label-container">
							<input id="urlPortal" name="urlRssNewsPortal" type="text" size="50" value="<c:out value="${editBean.urlRssNewsPortal}"></c:out>" >
							<br>
							<label class="form-sub-label">Введите URL, например, http://admomsk.ru/c/journal/get_articles?groupId=14&templateId=13236&delta=10</label>
						</span>
					</div>
				</li>
				
				<c:if test="${editBean.validUrlRssNewsPortal eq editBean.idle}">
					<li class="form-line">
		  				<div class="form-error-message for_host">
	  						Поле обязательно для заполнения.
	  					</div>
	  				</li>
				</c:if>
				
				<c:if test="${editBean.validUrlRssNewsPortal eq editBean.badUrl}">
					<li class="form-line">
		  				<div class="form-error-message for_host">
	  						Неверный URL.
	  					</div>
  					</li>
				</c:if>
				
			</ul>
			
			<ul class="form-section">
				<li class="form-line">
					<div class="form-input-wide" id="cid_2">
						<div class="form-buttons-wrapper" style="text-align:left">
							<button class="form-submit-button" type="submit" id="save_yandex_rss">
							Сохранить
							</button>
						</div>
					</div>
				</li>
			</ul>
			
		</div>
	</form>
</div>