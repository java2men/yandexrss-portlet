<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>

<portlet:defineObjects />

<%if ( (java.net.URL)renderRequest.getAttribute("urlRssNewsPortal") != null) { %>
<portlet:resourceURL var="varYandexRss"></portlet:resourceURL>
<p>Сгенерированный URL для Yandex RSS: <a href="<%=varYandexRss %>">YandexRss</a></p>
<%} else { %>
<p>URL не сгенерирован, проверьте настройки портлета</p>
<%}%>